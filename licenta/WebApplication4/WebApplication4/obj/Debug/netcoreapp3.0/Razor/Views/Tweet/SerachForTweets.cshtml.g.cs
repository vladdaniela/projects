#pragma checksum "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "35fb97fb8f878a468269ec8b00e9729e61e75ba5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Tweet_SerachForTweets), @"mvc.1.0.view", @"/Views/Tweet/SerachForTweets.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\_ViewImports.cshtml"
using WebApplication4;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\_ViewImports.cshtml"
using WebApplication4.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"35fb97fb8f878a468269ec8b00e9729e61e75ba5", @"/Views/Tweet/SerachForTweets.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9fff4eb847734ec2c3f91807e9b1a08bbda85e45", @"/Views/_ViewImports.cshtml")]
    public class Views_Tweet_SerachForTweets : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<WebApplication4.Models.SearchForTweetsModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/like.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("width: 60%;height: auto;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/dislike.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/i-dunno-lol.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("submit"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Back", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Tweet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery/dist/jquery.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/bootstrap/dist/js/bootstrap.bundle.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/report.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_10 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_11 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/chart/Chart.min.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 4 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
  
    ViewData["Title"] = "Serach For Tweets";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n    <div style=\"padding-left:5%;\">\r\n        <h1>");
#nullable restore
#line 10 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
       Write(Html.DisplayFor(model => model.Sentence));

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\r\n\r\n        <hr />\r\n        <div class=\"report\">\r\n            <div class=\"row\" style=\"font-weight: bold;padding-left:5%;\">\r\n                <br />\r\n\r\n                ");
#nullable restore
#line 17 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           Write(Html.DisplayNameFor(model => model.PositiveTweets));

#line default
#line hidden
#nullable disable
            WriteLiteral(":\r\n\r\n                ");
#nullable restore
#line 19 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           Write(Html.DisplayFor(model => model.PositiveTweets));

#line default
#line hidden
#nullable disable
            WriteLiteral("%<br />\r\n                <br />\r\n                ");
#nullable restore
#line 21 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           Write(Html.DisplayNameFor(model => model.NegativeTweets));

#line default
#line hidden
#nullable disable
            WriteLiteral(":\r\n\r\n                ");
#nullable restore
#line 23 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           Write(Html.DisplayFor(model => model.NegativeTweets));

#line default
#line hidden
#nullable disable
            WriteLiteral("%<br />\r\n                <br />\r\n\r\n                ");
#nullable restore
#line 26 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           Write(Html.DisplayNameFor(model => model.NeutralTweets));

#line default
#line hidden
#nullable disable
            WriteLiteral(":\r\n\r\n                ");
#nullable restore
#line 28 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           Write(Html.DisplayFor(model => model.NeutralTweets));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"%
            </div>

            <div id=""canvas-holder-pie"" style=""width:100%;"">
                <canvas style=""text-align:center;float:right;"" id=""chart-area-pie""></canvas>
            </div>

            <div id=""canvas-holder-chart"" style=""width:100%;"">
                <canvas style=""text-align:center;float:right;"" id=""chart-area-chart""></canvas>
            </div>
            <br />
        </div>
        
        <div id=""image-opinion"">
");
#nullable restore
#line 42 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
             if (Model.PositiveTweets > Model.NeutralTweets && Model.PositiveTweets > Model.NegativeTweets || Model.NegativeTweets == Model.NeutralTweets)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <div style=\"text-align:center;\" id=\"image\">\r\n                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "35fb97fb8f878a468269ec8b00e9729e61e75ba511092", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                </div>\r\n");
#nullable restore
#line 47 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
            }
            else if (Model.NegativeTweets > Model.PositiveTweets && Model.NegativeTweets > Model.NegativeTweets || Model.PositiveTweets == Model.NeutralTweets)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <div style=\"text-align:center;\" id=\"image\">\r\n                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "35fb97fb8f878a468269ec8b00e9729e61e75ba512716", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                </div>\r\n");
#nullable restore
#line 53 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
            }
            else if (Model.NeutralTweets > Model.NegativeTweets && Model.NeutralTweets > Model.PositiveTweets || Model.PositiveTweets == Model.NegativeTweets)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <div style=\"text-align:center;\" id=\"image\">\r\n                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "35fb97fb8f878a468269ec8b00e9729e61e75ba514339", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                </div>\r\n");
#nullable restore
#line 59 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("            <br />\r\n\r\n            <div id=\"opinion\">\r\n                ");
#nullable restore
#line 63 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           Write(Html.DisplayFor(model => model.Opinion));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n        <br />\r\n    </div>\r\n<div style=\"padding-left:5%;\">\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "35fb97fb8f878a468269ec8b00e9729e61e75ba516160", async() => {
                WriteLiteral("Back");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 69 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
                        WriteLiteral(Model.ProjectId);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</div>\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral(@"
    <script>
        var ctx = document.getElementById('chart-area-pie').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        Math.round(");
#nullable restore
#line 80 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
                              Write(Model.PositiveTweets);

#line default
#line hidden
#nullable disable
                WriteLiteral("),\r\n                        Math.round(");
#nullable restore
#line 81 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
                              Write(Model.NegativeTweets);

#line default
#line hidden
#nullable disable
                WriteLiteral("),\r\n                        Math.round(");
#nullable restore
#line 82 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
                              Write(Model.NeutralTweets);

#line default
#line hidden
#nullable disable
                WriteLiteral(@")  
                    ],
                    backgroundColor: [
                        '#F25CA2',
                        '#0433BF',
                        '#0B9ED9'
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Positive',
                    'Negative',
                    'Neutral'
                ]
            },
            options: {
                responsive: true
            }
        });       

          

");
#nullable restore
#line 104 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
           
            string a = Model.Labels.ElementAt(0).Month.ToString();
            foreach(var l in Model.Labels)
            {
                a +=","+l.Month.ToString();
            }            

            string b = Model.TweetSentiments.ElementAt(0).ToString();
            foreach(var d in Model.TweetSentiments)
            {
                b +=","+ d.ToString();
            }
        

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n          \r\n        var ctxLine = document.getElementById(\'chart-area-chart\').getContext(\'2d\');\r\n        var chartLine = new Chart(ctxLine, \r\n            {\r\n\t\t\t    type: \'line\',\r\n                    data: {\r\n                        labels: [");
#nullable restore
#line 124 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
                            Write(a);

#line default
#line hidden
#nullable disable
                WriteLiteral("],\r\n                    datasets: [{\r\n                                label: \'Time variation of tweets according to months\',\t\t\t\t\r\n\t\t\t\t\t    data: [");
#nullable restore
#line 127 "C:\Users\User\source\repos\WebApplication4\WebApplication4\Views\Tweet\SerachForTweets.cshtml"
                          Write(b);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"],
                        fill: false,
                        backgroundColor: ['#F25CA2'],
                        borderColor: ['#F25CA2'],
                        lineTension: 0.1
                        }]
                          
                },
                options: {                       
                    
                    scales: {
                        xAxes: {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        },
                        yAxes: {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Sentiment index'
                            }
                        }
                    }
                }
				
            });
              
");
                WriteLiteral("    </script>\r\n");
            }
            );
            WriteLiteral("\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "35fb97fb8f878a468269ec8b00e9729e61e75ba523103", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "35fb97fb8f878a468269ec8b00e9729e61e75ba524143", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "35fb97fb8f878a468269ec8b00e9729e61e75ba525183", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_10);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "35fb97fb8f878a468269ec8b00e9729e61e75ba526299", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_11);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<WebApplication4.Models.SearchForTweetsModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
