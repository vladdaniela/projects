﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication4.Data;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    [Authorize]
    public class AdministrationController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ApplicationDbContext context;

        public AdministrationController(UserManager<ApplicationUser> um, SignInManager<ApplicationUser> sm,IHostingEnvironment hostingEnvironment, ApplicationDbContext context)
        {
            this.userManager = um;
            this.signInManager = sm;
            this.hostingEnvironment = hostingEnvironment;
            this.context = context;
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.GetUserAsync(User);
                if (user == null)
                {
                    return RedirectToAction("Login", "Account");
                }

                // ChangePasswordAsync changes the user password
                var result = await userManager.ChangePasswordAsync(user,
                    model.CurrentPassword, model.NewPassword);

                // The new password did not meet the complexity rules or
                // the current password is incorrect. Add these errors to
                // the ModelState and rerender ChangePassword view
                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                    return View();
                }

                // Upon successfully changing the password refresh sign-in cookie
                await signInManager.RefreshSignInAsync(user);
                return View("ConfirmationPassword");
            }

            return View(model);
        }

        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if(user==null)
            {
                ViewBag.ErrorMessage = $"User with id ={id} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                foreach (var project in context.Projects.Where(x => x.UserId == user.Id))
                {
                    foreach (var word in context.Keywords.Where(x => x.ProjectId == project.ProjectId))
                    {
                        context.Keywords.Remove(word);
                    }
                    context.Projects.Remove(project);
                }
                var result=await userManager.DeleteAsync(user);
                context.SaveChanges();
                if(result.Succeeded)
                {
                    return RedirectToAction("Logout", "Account");
                }

                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
                return View("Logout", "Account");
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> EditUser(string id)
        {
            var user = await userManager.FindByIdAsync(id);
            if(user==null)
            {
                ViewBag.ErrorMessage = $"User with id = {id} cannot be found";
                return View("NOT FOUND");
            }

            var model = new EditViewModel
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.UserName
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(EditViewModel model)
        {
            
            var user = await userManager.FindByIdAsync(model.Id);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with id = {model.Id} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                string uniqueFileName = null;
                if (model.Picture != null)
                {
                    string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Picture.FileName;
                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    model.Picture.CopyTo(new FileStream(filePath, FileMode.Create));
                    user.Picture = uniqueFileName;

                }               
                user.Email = model.Email;
                user.UserName = model.UserName;

                var result =await userManager.UpdateAsync(user);
                if(result.Succeeded)
                {
                    return Redirect("/Account/MyProfile/"+user.Id);
                }

                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }           

            return View(model);
        }
    }
}