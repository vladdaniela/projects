﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication4.Data;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    [Authorize]
    public class KeywordController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ApplicationDbContext context;

        public KeywordController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddKeyword(AddWordsViewModel model)
        {
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == model.ProjectId);
            if (project == null)
            {
                ViewBag.ErrorMessage = $"User with id = {model.ProjectId} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var word = new Keyword
                    {
                        Word = model.Word,
                        ProjectId = model.ProjectId,
                        Project = project,
                        DateStarted = DateTime.Now,
                        Active = model.Active
                        
                    };
                    project.DateModified = DateTime.Now;

                    context.Projects.Update(project);
                    await context.Keywords.AddAsync(word);
                    await context.SaveChangesAsync();

                    if (word == null)
                        ViewData["word"] = "Word not added successfully!";
                    else if(word!=null)
                        return Redirect("/Project/Details/" + project.ProjectId);
                }

            }
            return View(model);
        }

        public IActionResult DeleteWord(string id)
        {
            int idFromRoute = int.Parse(id);
            var word = context.Keywords.FirstOrDefault(x => x.KeywordId == idFromRoute);
            string projectId = word.ProjectId.ToString();
            if (word == null)
            {
                ViewBag.ErrorMessage = $"Word with id ={id} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                context.Keywords.Remove(word);
                context.SaveChanges();

                if (context.Keywords.FirstOrDefault(x=>x.KeywordId==word.KeywordId)==null)
                {
                    return Redirect("/Project/Details/"+projectId);
                }

                return RedirectToAction("Index", "Home");
                
            }
        }

        public IActionResult ActivateOrDisable(string id)
        {
            int idFromRoute = int.Parse(id);
            var word = context.Keywords.FirstOrDefault(x => x.KeywordId == idFromRoute);
            string projectId = word.ProjectId.ToString();
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == word.ProjectId);
            if (word == null)
            {
                ViewBag.ErrorMessage = $"Word with id ={id} cannot be found";
                return View("NOT FOUND");
            }
            else
            {
                if(word.Active==true)
                {
                    word.Active = false;
                }
                else if(word.Active==false)
                {
                    word.Active = true;
                }
                project.DateModified = DateTime.Now;

                context.Keywords.Update(word);
                context.Projects.Update(project);
                context.SaveChanges();

                return Redirect("/Project/Details/" + projectId);              


            }
        }

        [HttpGet]
        public IActionResult AddKeyword(string id)
        {
            int idFromRoute = int.Parse(id);
            var project = context.Projects.FirstOrDefault(x => x.ProjectId == idFromRoute);
            if (project == null)
            {
                ViewBag.ErrorMessage = $"Poject with id = {id} cannot be found";
                return View("NOT FOUND");
            }

            AddWordsViewModel model = new AddWordsViewModel { ProjectId = project.ProjectId };

            return View(model);
        }
    }
}