﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class SearchForTweetsModel
    {
        public int ProjectId { get; set; }

        [Display(Name = "Positive")]
        public double PositiveTweets { get; set; }

        [Display(Name = "Negative")]
        public double NegativeTweets { get; set; }

        [Display(Name = "Neutral")]
        public double NeutralTweets { get; set; }
        public string Sentence { get; set; }
        public double[] TweetSentiments { get; set; }
        public DateTime[] Labels { get; set; }
        public string Opinion { get; set; }
    }
}
