﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
        public string Sentence { get; set; }
        public IEnumerable<Keyword> Keywords { get; set; }
    }
}
