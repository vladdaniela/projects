﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class Keyword_Tweet
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Keyword")]
        public int KeywordId { get; set; }
        public Keyword Keyword { get; set; }

        [ForeignKey("Tweet")]
        public int TweetId { get; set; }
        public Tweet Tweet { get; set; }
    }
}
