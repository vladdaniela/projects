﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class Keyword
    {
        [Key]
        public int KeywordId { get; set; }

        [Required(ErrorMessage = "Please enter at least a word")]
        [Display(Name = "Word")]
        public string Word { get; set; }
        public DateTime DateStarted { get; set; }
        public bool Active { get; set; }

        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public ICollection<Keyword_Tweet> Keyword_Tweets { get; set; }
    }
}
