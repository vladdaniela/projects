﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class AddWordsViewModel
    {

        [Required(ErrorMessage = "Please enter at least a word")]
        [Display(Name = "Word")]
        public string Word { get; set; }
        public bool Active { get; set; }
        public int ProjectId { get; set; }
    }
}
