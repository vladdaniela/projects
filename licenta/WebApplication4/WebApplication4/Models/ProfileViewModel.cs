﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class ProfileViewModel
    {
        public string Id { get; set; }

        [Required]
        public string UserName { get; set; }
        public string Picture { get; set; }
        public IEnumerable<Project> Projects { get; set; }
    }
}
