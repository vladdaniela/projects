﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Models
{
    public class Tweet
    {
        [Key]
        public int TweetId { get; set; }
        public DateTime Date { get; set; }
        public string Value { get; set; }
        public double Sentiment { get; set; }
        public ICollection<Keyword_Tweet> Keyword_Tweets { get; set; }
    }
}
