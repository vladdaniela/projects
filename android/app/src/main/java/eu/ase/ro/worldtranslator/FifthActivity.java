package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class FifthActivity extends AppCompatActivity {

    private Person person;
    private EditText id;
    private EditText firstName;
    private EditText lastName;
    private EditText mail;
    private EditText password;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);

        Intent intent=getIntent();
        person=intent.getParcelableExtra("personKey");

        id=findViewById(R.id.etId5);
        firstName=findViewById(R.id.etFName5);
        lastName=findViewById(R.id.etLName5);
        mail=findViewById(R.id.etEmail5);
        password=findViewById(R.id.etPassword5);
        db=new DatabaseHelper(this);

        id.setText(person.getId().toString());
        firstName.setText(person.getFirstName());
        lastName.setText(person.getLastName());
        mail.setText(person.getEmail());
        password.setText(person.getPasswordAuthentication());



    }

    public void btnUpdateeee(View view){


        String FName=firstName.getText().toString();
        person.setFirstName(FName);

        String LName=lastName.getText().toString();
        person.setLastName(LName);

        String Email =  mail.getText().toString();
        person.setEmail(Email);

        String pass=password.getText().toString();
        person.setPasswordAuthentication(pass);

        int i = db.updatePerson(person);
        Toast.makeText(FifthActivity.this,"Executed with code: "+i,Toast.LENGTH_LONG).show();

        Intent intent1=new Intent(this, ThirdActivity.class);
        intent1.putExtra("personKey", person);
        startActivity(intent1);
    }
}