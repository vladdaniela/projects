package eu.ase.ro.worldtranslator.model;

import android.os.Parcel;
import android.os.Parcelable;


import androidx.room.Entity;

import java.util.Date;


public class Sequence implements Parcelable {
    private Long id;
    private String content;//plain editText
    private String languageIn;//spinner
    private String translated;
    private String languageOut;//spinner
    private Integer noWords;//numeric editText
    private Float translationRating;//ratingBar
    private Boolean translationRecommended;//switch
    private Date translationDate;//date time picker

    public Sequence() {
    }



    public Sequence(Long id, String content, String languageIn, String translated, String languageOut, Integer noWords, Float translationRating, Boolean translationRecommended, Date translationDate) {
        this.id = id;
        this.content = content;
        this.languageIn = languageIn;
        this.translated = translated;
        this.languageOut = languageOut;
        this.noWords = noWords;
        this.translationRating = translationRating;
        this.translationRecommended = translationRecommended;
        this.translationDate = translationDate;
    }

    protected Sequence(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        content = in.readString();
        languageIn = in.readString();
        translated = in.readString();
        languageOut = in.readString();
        if (in.readByte() == 0) {
            noWords = null;
        } else {
            noWords = in.readInt();
        }
        if (in.readByte() == 0) {
            translationRating = null;
        } else {
            translationRating = in.readFloat();
        }
        byte tmpTranslationRecommended = in.readByte();
        translationRecommended = tmpTranslationRecommended == 0 ? null : tmpTranslationRecommended == 1;
    }

    public static final Creator<Sequence> CREATOR = new Creator<Sequence>() {
        @Override
        public Sequence createFromParcel(Parcel in) {
            return new Sequence(in);
        }

        @Override
        public Sequence[] newArray(int size) {
            return new Sequence[size];
        }
    };

    public Sequence(Long id, String content, String langIn, String result, String langOut, Integer noWords, boolean active) {
        this.id = id;
        this.content = content;
        this.languageIn = langIn;
        this.translated = result;
        this.languageOut = langOut;
        this.noWords = noWords;
        this.translationRecommended = active;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "Sequence{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", languageIn='" + languageIn + '\'' +
                ", translated='" + translated + '\'' +
                ", languageOut='" + languageOut + '\'' +
                ", noWords=" + noWords +
                ", translationRating=" + translationRating +
                ", translationRecommended=" + translationRecommended +
                ", translationDate=" + translationDate +
                '}';
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(id);
        }
        parcel.writeString(content);
        parcel.writeString(languageIn);
        parcel.writeString(translated);
        parcel.writeString(languageOut);
        if (noWords == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(noWords);
        }
        if (translationRating == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(translationRating);
        }
        parcel.writeByte((byte) (translationRecommended == null ? 0 : translationRecommended ? 1 : 2));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLanguageIn() {
        return languageIn;
    }

    public void setLanguageIn(String languageIn) {
        this.languageIn = languageIn;
    }

    public String getTranslated() {
        return translated;
    }

    public void setTranslated(String translated) {
        this.translated = translated;
    }

    public String getLanguageOut() {
        return languageOut;
    }

    public void setLanguageOut(String languageOut) {
        this.languageOut = languageOut;
    }

    public Integer getNoWords() {
        return noWords;
    }

    public void setNoWords(Integer noWords) {
        this.noWords = noWords;
    }

    public Float getTranslationRating() {
        return translationRating;
    }

    public void setTranslationRating(Float translationRating) {
        this.translationRating = translationRating;
    }

    public Boolean getTranslationRecommended() {
        return translationRecommended;
    }

    public void setTranslationRecommended(Boolean translationRecommended) {
        this.translationRecommended = translationRecommended;
    }

    public Date getTranslationDate() {
        return translationDate;
    }

    public void setTranslationDate(Date translationDate) {
        this.translationDate = translationDate;
    }
}