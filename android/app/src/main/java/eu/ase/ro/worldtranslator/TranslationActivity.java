package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.model.Sequence;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class TranslationActivity extends AppCompatActivity {

    private static final String TAG = TranslationActivity.class.getName();
    private EditText translated;
    private RatingBar rating;
    private Switch recommend;
    private Sequence sequence;
    private Person person;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation);

        translated=findViewById(R.id.etTr);
        rating=findViewById(R.id.ratingBar);
        recommend=findViewById(R.id.switch2);

        Intent intent=getIntent();
        sequence=intent.getParcelableExtra("sequenceKey");
        person=intent.getParcelableExtra("personKey");
        db=new DatabaseHelper(TranslationActivity.this);

    }

    public void btnD(View view)
    {
        String t=translated.getText().toString();
        sequence.setTranslated(t);

        float ratingB=rating.getRating();
        sequence.setTranslationRating(ratingB);

        boolean rec=recommend.isChecked();
        sequence.setTranslationRecommended(rec);

        long s_id = db.createSequence(sequence, db.creatingPerson(person));
        Toast.makeText(TranslationActivity.this,"Data inserted with id: "+s_id,Toast.LENGTH_LONG).show();

        Intent intent=new Intent(this, ForthActivity.class);
        intent.putExtra("sequenceKey", this.sequence);
        intent.putExtra("personKey",person );
        startActivity(intent);
    }
}
