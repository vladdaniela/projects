package eu.ase.ro.worldtranslator.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import java.util.HashMap;
import java.util.List;

import eu.ase.ro.worldtranslator.ForthActivity;
import eu.ase.ro.worldtranslator.R;
import eu.ase.ro.worldtranslator.model.Sequence;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class SequenceAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    private static final String TAG = SequenceAdapter.class.getName();
    private Context context;
    public List<Sequence> sequences;
    public HashMap<Long, Boolean> hashMap=new HashMap<>();
    DatabaseHelper db;

    public SequenceAdapter(Context context, List<Sequence> sequences) {
        this.context = context;
        this.sequences = sequences;
    }

    @Override
    public int getCount() {
        return sequences.size();
    }

    @Override
    public Object getItem(int position) {
        return sequences.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SequenceHolder sequenceHolder=null;
        if(convertView==null)
        {
            convertView= LayoutInflater
                    .from(context)
                    .inflate(R.layout.list_view_sequence_item, parent,false);
            sequenceHolder=new SequenceHolder(convertView);
            convertView.setTag(sequenceHolder);
        }
        else {
            sequenceHolder=(SequenceHolder)convertView.getTag();
        }
        Sequence sequence=(Sequence)getItem(position);
        //Log.d(TAG,"Date: "+sequence.getTranslationDate());
        sequenceHolder.idd.setText(sequence.getId().toString());
        sequenceHolder.tviContent.setText(sequence.getContent());
        sequenceHolder.tviLangIn.setText(sequence.getLanguageIn());
        sequenceHolder.tviLangOut.setText(sequence.getLanguageOut());
        sequenceHolder.tviNoWords.setText(sequence.getNoWords().toString());
        sequenceHolder.tviTranslated.setText(sequence.getTranslated());
        sequenceHolder.aSwitch.setChecked(sequence.getTranslationRecommended());

        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(context, "BaseAdapter: " + sequences.get(position).toString(), Toast.LENGTH_SHORT).show();
    }



//    @Override
//    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//        final int which_item=position;
//        new AlertDialog.Builder(context)
//                .setIcon(R.drawable.delete)
//                .setTitle("Are you sure?")
//                .setMessage("Do you want to delete this item?")
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Sequence s=(Sequence)getItem(which_item);
//                        db.deleteSequence(s.getId());
////                        sequences.remove(which_item);
////                        sequenceAdapter.notifyDataSetChanged();
//                    }
//                })
//                .setNegativeButton("No", null)
//                .show();
//
//        return true;
//    }

    private class SequenceHolder{
        public TextView idd;
        public TextView tviContent;
        public TextView tviLangIn;
        public TextView tviLangOut;
        public TextView tviTranslated;
        public TextView tviNoWords;
        public Switch aSwitch;

        SequenceHolder(View view){
            idd=view.findViewById(R.id.tvIdd);
            tviContent=view.findViewById(R.id.tvContent);
            tviLangIn=view.findViewById(R.id.tvLangIn);
            tviLangOut=view.findViewById(R.id.tvLangOut);
            tviTranslated=view.findViewById(R.id.tvTranslated);
            tviNoWords=view.findViewById(R.id.tvNoW);
            aSwitch=view.findViewById(R.id.switch1);
            aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Long i=Long.valueOf(idd.getText().toString());
                    Boolean value=isChecked;
                    hashMap.put(i,value);
                }
            });
        }
    }
}
