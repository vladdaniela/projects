package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import eu.ase.ro.worldtranslator.model.Person;

public class SecondActivity extends AppCompatActivity {

    private TextView nameSecond;
    private Person person;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        nameSecond=findViewById(R.id.NameSecond);
        Intent intent=getIntent();
        person=intent.getParcelableExtra("personKey");
        nameSecond.setText(person.getFirstName());
    }

    public void btnNext(View view)
    {
        Intent intent=new Intent(this, ThirdActivity.class);
        intent.putExtra("personKey", person);
        startActivity(intent);
    }

}
