package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import eu.ase.ro.worldtranslator.model.Sequence;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class EighthActivity extends AppCompatActivity {
    DatabaseHelper db;
    private ListView sequenceListView;
    private List<Sequence> sequenceList=new ArrayList<>();
    private static final String FILE_NAME="sequences.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eighth);

        Intent intent=getIntent();

        db=new DatabaseHelper(this);
        sequenceList=db.getAllSequences();

        sequenceListView = findViewById(R.id.list_view8);

        ArrayAdapter<Sequence> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, sequenceList);

        sequenceListView.setAdapter(arrayAdapter);


    }

    public void btnToFile(View view){
        FileOutputStream fos=null;

        try {
            fos=openFileOutput(FILE_NAME, MODE_PRIVATE);
            for(Sequence s:sequenceList){
                fos.write(s.getId().toString().getBytes());
                fos.write("\n".getBytes());
                fos.write(s.getContent().getBytes());
                fos.write("\n".getBytes());
                fos.write(s.getLanguageIn().getBytes());
                fos.write("\n".getBytes());
                fos.write(s.getLanguageOut().getBytes());
                fos.write("\n".getBytes());
                fos.write(s.getTranslated().getBytes());
                fos.write("\n".getBytes());
                fos.write(s.getNoWords().toString().getBytes());
                fos.write("\n".getBytes());
                fos.write(s.getTranslationRating().toString().getBytes());
                fos.write("\n".getBytes());
                fos.write(s.getTranslationRecommended().toString().getBytes());
                fos.write("\n".getBytes());
                fos.write("\n".getBytes());


            }
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Saved to "+getFilesDir()+"/"+FILE_NAME, Toast.LENGTH_LONG);

    }
}
