package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.model.Sequence;

public class SixthActivity extends AppCompatActivity {
    private Sequence sequence=new Sequence();
    private Person person;
    private EditText content;
    private Spinner langIn;
    private Spinner langOut;
    private EditText noWords;
    private EditText date;
    private EditText id;
    private AdapterView.OnItemSelectedListener itemSelectedListener=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String lIn=parent.getItemAtPosition(position).toString();
            Toast.makeText(getApplicationContext(), "Selected value: "+lIn, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener itemSelectedListener2=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String lOut=parent.getItemAtPosition(position).toString();
            Toast.makeText(getApplicationContext(), "Selected value: "+lOut, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sixth);

        Intent intent=getIntent();
        sequence = intent.getParcelableExtra("sequenceKey");
        person=intent.getParcelableExtra("personKey");

        content=findViewById(R.id.etContent6);
        langIn=findViewById(R.id.langInSpinner6);
        langOut=findViewById(R.id.langOutSpinner6);
        noWords=findViewById(R.id.etNoWords6);
        date=findViewById(R.id.etDate6);

        id=findViewById(R.id.etIDDDD6);

        langOut.setOnItemSelectedListener(itemSelectedListener2);
        langIn.setOnItemSelectedListener(itemSelectedListener);

        content.setText(sequence.getContent());
        noWords.setText(sequence.getNoWords());
       // date.setText(sequence.getTranslationDate().toString());
        id.setText(sequence.getId().toString());


    }

    public void btnTranslate6(View view){
        String lIn=langIn.getSelectedItem().toString();
        sequence.setLanguageIn(lIn);

        String lOut=langOut.getSelectedItem().toString();
        sequence.setLanguageOut(lOut);

        String cont=content.getText().toString();
        sequence.setContent(cont);

        int words;
        words=Integer.parseInt(noWords.getText().toString());
        sequence.setNoWords(words);


        Date date1= null;
        try {
            date1 = new SimpleDateFormat().parse(date.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sequence.setTranslationDate(date1);



        //  Log.d(TAG, "Date: "+sequence.getTranslationDate());


        Intent intent=new Intent(this, SeventhActivity.class);
        intent.putExtra("sequenceKey", sequence);
        intent.putExtra("personKey", person);
        startActivity(intent);
    }
}
