package eu.ase.ro.worldtranslator;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import eu.ase.ro.worldtranslator.adapters.SequenceAdapter;
import eu.ase.ro.worldtranslator.model.Sequence;

import static java.net.HttpURLConnection.HTTP_OK;

public class GetJSON extends AsyncTask<String, Void, String> {

    private static final String TAG = GetJSON.class.getName();
    private Handler mHandler;
    private SequenceAdapter mAdapter;
    private List<Sequence> sequences;

    public GetJSON(Handler mHandler, SequenceAdapter mAdapter, List<Sequence> sequences) {
        this.mHandler = mHandler;
        this.mAdapter = mAdapter;
        this.sequences=sequences;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String jsonFile) {
        super.onPostExecute(jsonFile);
        //parse json object and update the adapter content
        Log.d(TAG, jsonFile);
        try {
            JSONObject jsonObject = new JSONObject(jsonFile);
            JSONArray jsonArray = jsonObject.getJSONArray("dictionary ");
            for(int i=0; i<jsonArray.length(); i++)
            {
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                Long id = jsonItem.getLong("id");
                String content=jsonItem.getString("content");
                String langIn=jsonItem.getString("langIn");
                String langOut=jsonItem.getString("langOut");
                String result=jsonItem.getString("result");
                Integer noWords=jsonItem.getInt("noWords");
                boolean active = jsonItem.getBoolean("active");
                //for each of those jsonItems we need to update the adapters' collection
                mAdapter.hashMap.put(id,active);

                Sequence sequence=new Sequence(id, content, langIn, result, langOut, noWords, active);
//                sequence.setId(id);
//                sequence.setContent(content);
//                sequence.setLanguageIn(langIn);
//                sequence.setLanguageOut(langOut);
//                sequence.setTranslated(result);
//                sequence.setNoWords(noWords);
//                sequence.setTranslationRecommended(active);

                sequences.add(sequence);
                mAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected String doInBackground(String... strings) {
        String jsonFile = null;

        HttpURLConnection hcon = null;
        try {
            URL u = new URL(strings[0]);
            URLConnection con = u.openConnection();
            if (con instanceof HttpURLConnection) {
                hcon = (HttpURLConnection) con;
                hcon.connect();
                int resultCode = hcon.getResponseCode();
                if (resultCode == HTTP_OK) {
                    InputStream is = hcon.getInputStream();
                    //build the json file content from the IS
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int noBytes = 0;
                    while ((noBytes = is.read(buffer, 0, buffer.length)) != -1) {
                        baos.write(buffer, 0, noBytes);
                    }
                    jsonFile = baos.toString();

                    Message message = new Message();
                    Bundle bundle = new Bundle();
                    bundle.putString("json", jsonFile);
                    message.setData(bundle);
                    mHandler.handleMessage(message);

                }
            } else {
                throw new Exception("Invalid HTTP Connection");
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getLocalizedMessage());
        } finally {
            if (hcon != null)
                hcon.disconnect();
        }

        return jsonFile;
    }
}
