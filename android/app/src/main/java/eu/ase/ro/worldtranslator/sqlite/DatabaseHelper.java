package eu.ase.ro.worldtranslator.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.model.Sequence;

public class DatabaseHelper extends SQLiteOpenHelper  {
    private static final String LOG = "DatabaseHelper";


    public static final String DATABASE_NAME="translator.db";

    public static final String PERSON_TABLE="Person";
    public static final String Col1_id="ID";
    public static final String Col2_firstName="FIRST_NAME";
    public static final String Col3_lastName="LAST_NAME";
    public static final String Col4_email="EMAIL";
    public static final String Col5_password="PASSWORD";

    public static final String SEQUENCE_TABLE="Sequence";
    public static final String Col1s_id="ID";
    public static final String Col2_content="CONTENT";
    public static final String Col3_langIn="LANG_IN";
    public static final String Col4_langOut="LANG_OUT";
    public static final String Col5_result="TRANSLATED";
    public static final String Col6_noWords="NO_WORDS";
    public static final String Col7_rating="RATING";
    public static final String Col8_recommend="RECOMMEND";
    public static final String Col9_date="DATE";


    public static final String CONNECTION_TABLE="Connection";
    public static final String Col1c_id="ID";
    public static final String Col2p_id="PERSON_ID";
    public static final String Col3s_id="SEQUENCE_ID";





    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, 4);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        String person_table="create table " + PERSON_TABLE +" (ID INTEGER PRIMARY KEY,FIRST_NAME TEXT,LAST_NAME TEXT,EMAIL TEXT, PASSWORD TEXT)";
        String sequence_table="create table " + SEQUENCE_TABLE + " (ID INTEGER PRIMARY KEY,CONTENT TEXT,LANG_IN TEXT,LANG_OUT TEXT, TRANSLATED TEXT, NO_WORDS INTEGER, RATING REAL, RECOMMEND NUMERIC, DATE TEXT)";
        String connection_table="create table " + CONNECTION_TABLE +" (ID INTEGER PRIMARY KEY AUTOINCREMENT, PERSON_ID INTEGER, SEQUENCE_ID INTEGER)";
        db.execSQL(person_table);
        db.execSQL(sequence_table);
        db.execSQL(connection_table);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+PERSON_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+SEQUENCE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+CONNECTION_TABLE);
        onCreate(db);
    }







    //creating an sequence and asigning it to a person

    public long createSequence(Sequence s, long person_id){
        SQLiteDatabase db =this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(Col1s_id, s.getId());
        contentValues.put(Col2_content,s.getContent());
        contentValues.put("LANG_IN", s.getLanguageIn());
        contentValues.put("LANG_OUT", s.getLanguageOut());
        contentValues.put("TRANSLATED", s.getTranslated());
        contentValues.put("NO_WORDS", s.getNoWords());
        contentValues.put("RATING", s.getTranslationRating());
        contentValues.put("RECOMMEND", s.getTranslationRecommended());
       // contentValues.put(Col9_date, s.getTranslationDate().toString());



        //insert row
        long s_id = db.insert(SEQUENCE_TABLE,null,contentValues);




        createJoinTableRow(s_id,person_id);
        db.close();

        return s_id;
    }




    //fetching a single sequence

    public Sequence getSequence(long s_id){
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + SEQUENCE_TABLE + " WHERE "
                + Col1s_id + " = " + s_id;

       // Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery,null);

        if(c != null){
            c.moveToFirst();
        }

        Sequence s = new Sequence();

        s.setId(c.getLong(c.getColumnIndex(Col1s_id)));
        s.setContent(c.getString(c.getColumnIndex(Col2_content)));
        s.setLanguageIn(c.getString(c.getColumnIndex(Col3_langIn)));
        s.setLanguageOut(c.getString(c.getColumnIndex(Col4_langOut)));
        s.setTranslated(c.getString(c.getColumnIndex(Col5_result)));
        s.setTranslationRecommended(Boolean.valueOf(c.getString(c.getColumnIndex(Col8_recommend))));
        s.setTranslationRating(Float.valueOf(c.getString(c.getColumnIndex(Col7_rating))));
        s.setNoWords(Integer.valueOf(c.getString(c.getColumnIndex(Col6_noWords))));
      //  s.setTranslationDate(Date.valueOf(c.getString(c.getColumnIndex(Col9_date))));


        return s;

    }






    //fetching all sequences involves, reading all sequence row and adding them to a list array

    public List<Sequence> getAllSequences() {

        List<Sequence>  sequenceList= new ArrayList<>();
        String selectQuery = "SELECT * FROM " + SEQUENCE_TABLE;

        Log.e(LOG,selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery,null);

        if(c.moveToFirst()){
            do{


                Sequence s = new Sequence();
                s.setId(c.getLong(c.getColumnIndex(Col1s_id)));
                s.setContent(c.getString(c.getColumnIndex(Col2_content)));
                s.setLanguageIn(c.getString(c.getColumnIndex(Col3_langIn)));
                s.setLanguageOut(c.getString(c.getColumnIndex(Col4_langOut)));
                s.setTranslated(c.getString(c.getColumnIndex(Col5_result)));
                s.setTranslationRecommended(Boolean.valueOf(c.getString(c.getColumnIndex(Col8_recommend))));
                s.setTranslationRating(Float.valueOf(c.getString(c.getColumnIndex(Col7_rating))));
                s.setNoWords(Integer.valueOf(c.getString(c.getColumnIndex(Col6_noWords))));
         //       s.setTranslationDate(Date.valueOf(c.getString(c.getColumnIndex(Col9_date))));

                sequenceList.add(s);

            } while (c.moveToNext());
        }

        return sequenceList;
    }



    //Fetching all sequences under a person name

    public List<Sequence> getAllSequencesByPerson(String lastname){

        List<Sequence> sequenceList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + SEQUENCE_TABLE + " st, "
                + PERSON_TABLE + " pt," + CONNECTION_TABLE + " ct WHERE pt."
                + Col3_lastName + " = '" + lastname + "'" + " AND pt." + Col1_id
                + " = " + " ct." + Col2p_id + " AND st." + Col1s_id + " = "
                + "ct." + Col3s_id;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery,null);

        //looping through all rows and adding to list
        if(c.moveToFirst()){
            do{

                Sequence s = new Sequence();
                s.setId(c.getLong(c.getColumnIndex(Col1s_id)));
                s.setContent(c.getString(c.getColumnIndex(Col2_content)));
                s.setLanguageIn(c.getString(c.getColumnIndex(Col3_langIn)));
                s.setLanguageOut(c.getString(c.getColumnIndex(Col4_langOut)));
                s.setTranslated(c.getString(c.getColumnIndex(Col5_result)));
                s.setTranslationRecommended(Boolean.valueOf(c.getString(c.getColumnIndex(Col8_recommend))));
                s.setTranslationRating(Float.valueOf(c.getString(c.getColumnIndex(Col7_rating))));
                s.setNoWords(Integer.valueOf(c.getString(c.getColumnIndex(Col6_noWords))));
             //   s.setTranslationDate(Date.valueOf(c.getString(c.getColumnIndex(Col9_date))));

                //adding to sequence object to list

                sequenceList.add(s);
            }while (c.moveToNext());
        }
        return sequenceList;
    }




    //updating a sequence

    public int updateSequence(Sequence sequence){

        SQLiteDatabase db =this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Col1s_id, sequence.getId());
        contentValues.put(Col2_content,sequence.getContent());
        contentValues.put(Col3_langIn, sequence.getLanguageIn());
        contentValues.put(Col4_langOut, sequence.getLanguageOut());
        contentValues.put(Col5_result, sequence.getTranslated());
        contentValues.put(Col6_noWords, sequence.getNoWords());
        contentValues.put(Col7_rating, sequence.getTranslationRating());
        contentValues.put(Col8_recommend, sequence.getTranslationRecommended());
     //   contentValues.put(Col9_date, sequence.getTranslationDate().toString());

        return db.update(SEQUENCE_TABLE,contentValues, Col1s_id + " =?",
                new String[] {String.valueOf(sequence.getId())});

    }

    //deleting a sequence

    public void deleteSequence(long s_id){
        SQLiteDatabase db =this.getWritableDatabase();

        ContentValues contentValues =new ContentValues();
        db.delete(SEQUENCE_TABLE, Col1s_id + " =?",
                new String[] {String.valueOf(s_id)});
    }



    //CREATING PERSON


    public long creatingPerson(Person person){

        SQLiteDatabase db =this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(Col1_id, person.getId());
        contentValues.put(Col2_firstName,person.getFirstName());
        contentValues.put(Col3_lastName,person.getLastName());
        contentValues.put(Col4_email,person.getEmail());
        contentValues.put(Col5_password,person.getPasswordAuthentication());



        //insert row

        long person_id = db.insert(PERSON_TABLE,null,contentValues);
        db.close();
        return person_id;

    }


    //fetching all persons

    public List<Person> getAllPersons(){

        List<Person> personList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + PERSON_TABLE;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery,null);

        if(c.moveToFirst()){
            do {
                Person p = new Person();

                p.setId((c.getLong(c.getColumnIndex(Col1_id))));
                p.setLastName(c.getString(c.getColumnIndex(Col3_lastName)));
                p.setFirstName(c.getString(c.getColumnIndex(Col2_firstName)));
                p.setPasswordAuthentication(c.getString(c.getColumnIndex(Col5_password)));
                p.setEmail(c.getString(c.getColumnIndex(Col4_email)));

                personList.add(p);

            } while (c.moveToNext());
        }
        return personList;
    }

    //fetching a specific person

    public List<Person> getPerson(String name){

        List<Person> personList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + PERSON_TABLE + " WHERE " + Col2_firstName + " = '" + name + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery,null);

        if(c.moveToFirst()){
            do {
                Person p = new Person();

                p.setId((c.getLong(c.getColumnIndex(Col1_id))));
                p.setLastName(c.getString(c.getColumnIndex(Col3_lastName)));
                p.setFirstName(c.getString(c.getColumnIndex(Col2_firstName)));
                p.setPasswordAuthentication(c.getString(c.getColumnIndex(Col5_password)));
                p.setEmail(c.getString(c.getColumnIndex(Col4_email)));

                personList.add(p);

            } while (c.moveToNext());
        }
        return personList;
    }

    public List<Sequence> getSequence( String content){

        List<Sequence> sequenceList =  new ArrayList<>();

        String selectQuery = "SELECT * FROM " + SEQUENCE_TABLE + " WHERE " + Col2_content + " = '" + content + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery,null);

        if(c.moveToFirst()){
            do {
                Sequence s  = new Sequence();

                s.setId(c.getLong(c.getColumnIndex(Col1s_id)));
                s.setContent(c.getString(c.getColumnIndex(Col2_content)));
                s.setLanguageIn(c.getString(c.getColumnIndex(Col3_langIn)));
                s.setLanguageOut(c.getString(c.getColumnIndex(Col4_langOut)));
                s.setTranslated(c.getString(c.getColumnIndex(Col5_result)));
                s.setTranslationRecommended(Boolean.valueOf(c.getString(c.getColumnIndex(Col8_recommend))));
                s.setTranslationRating(Float.valueOf(c.getString(c.getColumnIndex(Col7_rating))));
                s.setNoWords(Integer.valueOf(c.getString(c.getColumnIndex(Col6_noWords))));
            //    s.setTranslationDate(Date.valueOf(c.getString(c.getColumnIndex(Col9_date))));

                sequenceList.add(s);

            } while (c.moveToNext());
        }
        return sequenceList;


    }


    //updating persons

    public int updatePerson(Person person){
        SQLiteDatabase db =this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();


      //  contentValues.put(Col1_id, person.getId());
        contentValues.put(Col2_firstName,person.getFirstName());
        contentValues.put(Col3_lastName,person.getLastName());
        contentValues.put(Col4_email,person.getEmail());
        contentValues.put(Col5_password,person.getPasswordAuthentication());



        return  db.update(PERSON_TABLE,contentValues,Col1_id + " =?",
                new String[] {String.valueOf(person.getId())});
    }


    //deleting person and the sequences under the person
    // should_delete_all_sequences_person = Passing true will delete all the sequences under the last name

    public void deleteAllSequencesPerPerson(Person person, boolean should_delete_all_sequences_per_person){
        SQLiteDatabase db =this.getWritableDatabase();

        if(should_delete_all_sequences_per_person){
            //get all IPs under this user
            List<Sequence> allSequencesByPerson = getAllSequencesByPerson(person.getLastName());

            for(Sequence s : allSequencesByPerson){
                deleteSequence(s.getId());
            }
        }

        deleteConnectionTableRow(person.getId());

        db.delete(PERSON_TABLE,Col1_id + " =?",
                new String[] { String.valueOf(person.getId())});
    }




    //check if person already exists

    public boolean checkPerson(String personName, String password){

        String[] columns = { Col1_id, Col2_firstName, Col4_email};
        SQLiteDatabase db = getReadableDatabase();
        String selection = Col3_lastName + "=?" + " and " + Col5_password + "=?";
        String[] selectionArgs = {personName, password};

        Cursor cursor = db.query(PERSON_TABLE,columns,selection,selectionArgs,null,null,null);
        int count = cursor.getCount();
        db.close();

        if(count >  0)
            return true;
        else
            return false;

    }


    //Below are the methods to access the rows from CONNECTION_TABLE table


    //Assigning a person to sequence


    //creating join table between person and sequence

    public long createJoinTableRow(long s_id, long person_id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();


        contentValues.put(Col3s_id,s_id);
        contentValues.put(Col2p_id,person_id);

        long id = db.insert(CONNECTION_TABLE,null,contentValues);
        return id;
    }


    //updating a person of sequence
    //updating a row in the connection table


    public int updatePerson(long id, long person_id){

        SQLiteDatabase db =this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Col1_id,person_id);

        return db.update(CONNECTION_TABLE,contentValues, Col1c_id + " =?",
                new String[] { String.valueOf(id)});
    }



    //Deleting a row in the connection table

    public void deleteConnectionTableRow(long p_id){
        SQLiteDatabase db =this.getWritableDatabase();

        db.delete(CONNECTION_TABLE,Col2p_id + " =?",
                new String[] { String.valueOf(p_id)});


    }


    //Closing a database

    public void closeDB(){
        SQLiteDatabase db = this.getReadableDatabase();
        if(db!=null && db.isOpen()){
            db.close();

        }
    }



}
