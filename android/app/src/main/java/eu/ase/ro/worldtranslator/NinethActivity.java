package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.model.Sequence;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class NinethActivity extends AppCompatActivity {
    private static final String FILE_NAME="persons.txt";
    DatabaseHelper db;
    private ListView personListView;
    private List<Person> personList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nineth);

        Intent intent=getIntent();

        db=new DatabaseHelper(this);
        personList=db.getAllPersons();

        personListView = findViewById(R.id.list_view9);

        ArrayAdapter<Person> arrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, personList);

        personListView.setAdapter(arrayAdapter);
    }

    public void btnToFileP(View view){
        FileOutputStream fos=null;

        try {
            fos=openFileOutput(FILE_NAME, MODE_PRIVATE);
            for(Person p:personList){
                fos.write(p.getId().toString().getBytes());
                fos.write("\n".getBytes());
                fos.write(p.getFirstName().getBytes());
                fos.write("\n".getBytes());
                fos.write(p.getLastName().getBytes());
                fos.write("\n".getBytes());
                fos.write(p.getEmail().getBytes());
                fos.write("\n".getBytes());
                fos.write(p.getPasswordAuthentication().getBytes());
                fos.write("\n".getBytes());
                fos.write("\n".getBytes());
            }
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Saved to "+getFilesDir()+"/"+FILE_NAME, Toast.LENGTH_LONG);
    }
}
