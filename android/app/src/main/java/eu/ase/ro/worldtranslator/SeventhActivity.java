package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.Toast;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.model.Sequence;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class SeventhActivity extends AppCompatActivity {
    private EditText translated;
    private RatingBar rating;
    private Switch recommend;
    private Sequence sequence;
    private Person person;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seventh);
        Intent intent=getIntent();
        sequence=intent.getParcelableExtra("sequenceKey");
        person=intent.getParcelableExtra("personKey");
        db=new DatabaseHelper(this);

        translated=findViewById(R.id.etTr7);
        rating=findViewById(R.id.ratingBar7);
        recommend=findViewById(R.id.switch27);

        rating.setRating(sequence.getTranslationRating());
        recommend.setChecked(sequence.getTranslationRecommended());
    }

    public void btnUpdate7(View view){
        String t=translated.getText().toString();
        sequence.setTranslated(t);

        float ratingB=rating.getRating();
        sequence.setTranslationRating(ratingB);

        boolean rec=recommend.isChecked();
        sequence.setTranslationRecommended(rec);

        int i = db.updateSequence(sequence);
        Toast.makeText(SeventhActivity.this,"Executed with code: "+i,Toast.LENGTH_LONG).show();

        Intent intent=new Intent(this, ForthActivity.class);
        intent.putExtra("sequenceKey", sequence);
        intent.putExtra("personKey", person);
        startActivity(intent);
    }
}
