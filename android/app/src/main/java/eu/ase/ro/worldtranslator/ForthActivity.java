package eu.ase.ro.worldtranslator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import eu.ase.ro.worldtranslator.adapters.SequenceAdapter;
import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.model.Sequence;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class ForthActivity extends AppCompatActivity {
    private static List<Sequence> sequenceList=new ArrayList<>();
    private ListView sequenceListView;
    private Sequence sequence;
    private Handler handler;
    private SequenceAdapter sequenceAdapter;
    private Person person;
    DatabaseHelper db;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                //call for the async task
                GetJSON getJson = new GetJSON(handler, sequenceAdapter, sequenceList);
                //now is when the doInBackground method is called upo
                getJson.execute("https://api.myjson.com/bins/6zics");
                return true;
            case R.id.addd:
                Intent intent=new Intent(this, ThirdActivity.class);
                intent.putExtra("personKey", person);
                startActivity(intent);
                return true;
            case R.id.s_report:
                Intent intent1=new Intent(this, EighthActivity.class);
                startActivity(intent1);
                return true;
            case R.id.p_report:
                Intent intent2=new Intent(this, NinethActivity.class);
                startActivity(intent2);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info= (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        sequence=(Sequence)sequenceListView.getSelectedItem();

        switch (item.getItemId()){
            case R.id.update:
                Intent intent=new Intent(this, SixthActivity.class);
                intent.putExtra("sequenceKey", sequence);
                intent.putExtra("personKey", person);
                startActivity(intent);
                return true;
            case R.id.delete:
                db.deleteSequence(sequence.getId());
                sequenceList.remove(info.position);
                sequenceAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Sequence with id: " + sequence.getId()+" was deleted", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forth);

        Intent intent=getIntent();
        sequence=intent.getParcelableExtra("sequenceKey");
        person=intent.getParcelableExtra("personKey");


        if(sequence!=null)
        {
            sequenceList.add(sequence);
        }

        db=new DatabaseHelper(this);

        sequenceListView = findViewById(R.id.lvSequence);

//        ArrayAdapter<Sequence> arrayAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_list_item_1, sequenceList);
//
//        sequenceListView.setAdapter(arrayAdapter);

        sequenceAdapter= new SequenceAdapter(this, sequenceList);
        sequenceListView.setAdapter(sequenceAdapter);

        sequenceListView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "ForthActivity:" + sequenceList.get(position).toString(), Toast.LENGTH_SHORT).show();
            }
        });

//        sequenceListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(final AdapterView<?> parent, View view, int position, long id) {
//                final int which_item=position;
//                new AlertDialog.Builder(ForthActivity.this)
//                        .setIcon(R.drawable.delete)
//                        .setTitle("Are you sure?")
//                        .setMessage("Do you want to delete this item?")
//                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Sequence s=(Sequence)parent.getItemAtPosition(which_item);
//                                db.deleteSequence(s.getId());
//                                sequenceList.remove(which_item);
//                                sequenceAdapter.notifyDataSetChanged();
//                            }
//                        })
//                        .setNegativeButton("No", null)
//                        .show();
//
//                return true;
//            }
//        });


        handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {
                Bundle data = msg.getData();
                final String json = data.getString("json");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getBaseContext(),
                                "Message: " + json,
                                Toast.LENGTH_SHORT).show();
                    }
                });


            }
        };

        registerForContextMenu(sequenceListView);


    }


}
