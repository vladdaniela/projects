package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = SecondActivity.class.getName();
    private EditText id;
    private EditText firstName;
    private EditText lastName;
    private EditText mail;
    private EditText password;
    DatabaseHelper db;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String FName = "fnameKey";
    public static final String LName = "lnameKey";
    public static final String Email = "emailKey";

    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstName=findViewById(R.id.etFName);
        lastName=findViewById(R.id.etLName);
        mail=findViewById(R.id.etEmail);
        password=findViewById(R.id.etPassword);
        id=findViewById(R.id.etId);
        sharedpreferences=getSharedPreferences(MyPREFERENCES,Context.MODE_PRIVATE);
        db=new DatabaseHelper(this);
    }

    public void btnSave(View view){
        String fn  = firstName.getText().toString();
        String ln  = lastName.getText().toString();
        String e  = mail.getText().toString();

        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString(FName, fn);
        editor.putString(LName, ln);
        editor.putString(Email, e);
        editor.commit();
        Toast.makeText(MainActivity.this,"Thanks",Toast.LENGTH_LONG).show();
    }

    public void btnStart(View view)
    {
        Person person=new Person();

        Long idd=Long.valueOf(id.getText().toString());
        person.setId(idd);

        String FName=firstName.getText().toString();
        person.setFirstName(FName);

        String LName=lastName.getText().toString();
        person.setLastName(LName);

        String Email =  mail.getText().toString();
        person.setEmail(Email);

        String pass=password.getText().toString();
        person.setPasswordAuthentication(pass);



        //Log.d(TAG, "Name: "+person.getName());
        Intent intent=new Intent(this, SecondActivity.class);
        intent.putExtra("personKey", person);
        startActivity(intent);
    }
}
