package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.sqlite.DatabaseHelper;

public class PersonActivity extends AppCompatActivity {

    private TextView fn;
    private TextView ln;
    private TextView m;
    private Person person;
    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        Intent intent=getIntent();
        person=intent.getParcelableExtra("personKey");

        db=new DatabaseHelper(this);

        fn=findViewById(R.id.FName);
        ln=findViewById(R.id.LName);
        m=findViewById(R.id.mail);

        fn.setText(person.getFirstName());
        ln.setText(person.getLastName());
        m.setText(person.getEmail());
    }

    public void btnLogOut(View view)
    {
        db.deleteAllSequencesPerPerson(person, true);
        Intent intent=new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void btnEdit(View view)
    {
        Intent intent=new Intent(this, FifthActivity.class);
        intent.putExtra("personKey", person);
        startActivity(intent);
    }
}
