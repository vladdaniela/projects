package eu.ase.ro.worldtranslator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import eu.ase.ro.worldtranslator.model.Person;
import eu.ase.ro.worldtranslator.model.Sequence;

public class ThirdActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private static final String TAG = ThirdActivity.class.getName();
    private Sequence sequence=new Sequence();
    private Person person;
    private TextView nameP;
    private EditText content;
    private Spinner langIn;
    private Spinner langOut;
    private EditText noWords;
    private EditText date;
    private EditText id;
    private AdapterView.OnItemSelectedListener itemSelectedListener=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String lIn=parent.getItemAtPosition(position).toString();
            Toast.makeText(getApplicationContext(), "Selected value: "+lIn, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private AdapterView.OnItemSelectedListener itemSelectedListener2=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String lOut=parent.getItemAtPosition(position).toString();
            Toast.makeText(getApplicationContext(), "Selected value: "+lOut, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Intent intent=getIntent();
        person = intent.getParcelableExtra("personKey");
        nameP=findViewById(R.id.tvFirst);
        nameP.setText(person.getFirstName()+",");

        content=findViewById(R.id.etContent);
        langIn=findViewById(R.id.langInSpinner);
        langOut=findViewById(R.id.langOutSpinner);
        noWords=findViewById(R.id.etNoWords);
        date=findViewById(R.id.etDate);

        id=findViewById(R.id.etIDDDD);

        langOut.setOnItemSelectedListener(itemSelectedListener2);
        langIn.setOnItemSelectedListener(itemSelectedListener);

    }

    public void btnDate(View view)
    {
        DialogFragment datePicker=new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "date picker");
    }



    public void btnTranslate(View view) {
        Long i=Long.valueOf(id.getText().toString());
        sequence.setId(i);

        String lIn=langIn.getSelectedItem().toString();
        sequence.setLanguageIn(lIn);

        String lOut=langOut.getSelectedItem().toString();
        sequence.setLanguageOut(lOut);

        String cont=content.getText().toString();
        sequence.setContent(cont);

        int words;
        words=Integer.parseInt(noWords.getText().toString());
        sequence.setNoWords(words);


        Date date1= null;
        try {
            date1 = new SimpleDateFormat().parse(date.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sequence.setTranslationDate(date1);



      //  Log.d(TAG, "Date: "+sequence.getTranslationDate());


        Intent intent=new Intent(this, TranslationActivity.class);
        intent.putExtra("sequenceKey", sequence);
        intent.putExtra("personKey", person);
        startActivity(intent);


    }

    public void btnAccountDetails(View view)
    {
        Intent intent=new Intent(this, PersonActivity.class);
        intent.putExtra("personKey", person);
        startActivity(intent);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        Calendar calendar=Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString= DateFormat.getDateInstance(DateFormat.SHORT).format(calendar.getTime());
        date.setText(currentDateString);
    }
}
