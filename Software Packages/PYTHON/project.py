import pandas as pd
import matplotlib.pyplot as plt

#1. Read the csv named 'jewellery_data' and
# put it in a dataframe using pandas.
df=pd.read_csv(r'C:\Users\User\PycharmProjects\ceva\jewellery_data.csv', sep=';')
print(df)

#2. Make a function that calculates the
# total value of the order (price*quantity).
# Use it and create a new column in the dataframe
# called 'total_value'.

def total_value(price, quantity):
    value=price*quantity
    return value

df['total_value']=total_value(df['price'],df['quantity'])
print(df['total_value'])

#3. Make a function that calculates the discount value for an order.
# If the total_value is less than 500, the discount is 0.
# If the total value is between 500 and 2000, the discount is 20%
# from the total value.
# If the total value is bigger than 2000 then is is 30%.

def discount():
    df['discount']=0
    for i in df['total_value']:
        if i in [500, 2000]:
            df['discount']=df['total_value']*0.2
        elif i>2000:
            df['discount']=df['total_value']*0.3

discount()
print(df['discount'])


#4. Change the value in the column named 'fidelity' with 5 if the total value
# of an order is bigger than 5000 and the jewellery type is bracelet
# and with 3 if the total value is less or equal with 5000.

df.loc[(df['total_value']>5000 & (df['jewellery_type']=='bracelet')),['fidelity']]=5
df.loc[(df['total_value']<=5000),['fidelity']]=3
print(df['fidelity'])

#5. Modify the price of a ring so that it rises with 5.6% of the old price.
print(df['price'][df['jewellery_type']=='ring'])
df.loc[(df['jewellery_type']=='ring'), ['price']]=df['price']+df['price']*0.056
print(df['price'][df['jewellery_type']=='ring'])

#6. Create a function that selects the first 5 orders from the csv
# and prints the emails of the clients of each order.

def gift():
    X=df.iloc[0:5]
    for i in X['quantity']:
        if i>=5:
            print(X['email'])
            print('Congrats! You received a gift!')

gift()

#7. Delete the clients from the csv that canceled their order (state = canceled).

print(df['state'])
df = df.drop(df['client_name'][df['state']=='canceled'].index)
print(df['state'])

#8. The offer is no longer available. That means that you
# should delete the column named 'discount'
# because our store does not present this decrease in price anymore.

print(df)
df.drop(["discount"], axis = 1, inplace = True)
print(df)


#9. Represent on a bar chart the total value bigger than 5000 of each client.

plot_data=df[df['total_value']>5000]
plot_data=plot_data.groupby('client_name')['total_value'].sum()
plot_data.plot(kind='bar', color = 'lightpink')
plt.show()


#10. Create a dictionary with data regarding the material
# from which the jewellery is made with their specific time in hours of preparation.
# After that modify the time of the preparation of emerald and diamond to 30.

myDict = {'Diamond' : 20, 'Gold':10, 'Emerald':15, 'Silver':5}
print(myDict)
myDict2=myDict.fromkeys(['Gold', 'Emerald'], 30)
print(myDict2.get('Gold'))
print(myDict2.get('Emerald'))








