*ex1;
data TOTAL;
	set work.import(keep=  order_no price quantity jewellery_type); 
	select;
	when(jewellery_type='ring') Total=price*quantity;
	otherwise Total=0;
	end;
run;
proc print data=TOTAL;
run;

*ex2;
data rings;
set work.total;
if jewellery_type IN ("ring");
run;
proc print data=rings;
run;

*ex3;
proc format;
value		total low-1000 = '<1000'
		 1000-4000 = '1000-4000'
		 4000-high='>4000';
		run;

title "The appearance frequency for total amount of rings orderds";
proc FREQ data=work.total;
	TABLES Total /nocum nopercent;
       	FORMAT Total total.;
run;

*ex4;
data TOTAL_ORDERS;
	set work.import(keep= order_no price quantity client_name jewellery_type);	
	do;
	Total=price*quantity;	
	end;
	put Total= ;
run;
proc report data=TOTAL_ORDERS;
run;

*ex5;
proc gplot data=TOTAL_ORDERS;
plot Total*client_name;
run;
quit;

*ex6;
data total_orders_discount;
set work.total_orders;
if jewellery_type IN ("bracelet");
do;
Discount=0.3;
end;
put Discount= ;
run;
proc report data=total_orders_discount;
run;

*ex7;
data total_discount_calculcated;
set work.total_orders_discount;
do;
Total_Discount=Total-(Discount*Total);
end;
put Total_Discount= ;
run;
proc report data=total_discount_calculcated;
run;

*ex8;
proc gplot data=total_discount_calculcated;
plot Total_Discount*client_name;
run;
quit;

*ex9;
proc format;
value		Total_Discount low-1000 = '<1000'
		 1000-4000 = '1000-4000'
		 4000-high='>4000';
		run;

title "The appearance frequency for final total amount of bracelet orderds";
proc FREQ data=work.total_discount_calculcated;
	TABLES Total_Discount /nocum nopercent;
       	FORMAT Total_Discount Total_Discount.;
run;

*ex10;
data report;
set work.total_discount_calculcated;
if quantity=5 then
Type='MAXIMUM';
else
Type='NORMAL';
run;
proc print data=report;
run;